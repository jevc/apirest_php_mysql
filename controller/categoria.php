<?php

    header('Content-Type:application/json');

    require("../config/conexion.php");
    require("../models/categoria.php");
    $categoria = new Categoria();

    $body = json_decode(file_get_contents("php://input"), true);

    switch ( $_GET["opc"] ) {

        case "getAll" :
            $datos = $categoria->get_categoria();
            echo json_encode($datos);
        break;
        case "getId":
            $datos = $categoria->get_categoria_x_id($body["cat_id"]);
            echo json_encode($datos);
        break;
        case "insert":
            $datos = $categoria->insert_categoria($body["cat_nom"], $body["cat_obs"]);
            echo "Insert Correcto";
        break;
        case "update":
            $datos = $categoria->update_categoria($body["cat_id"], $body["cat_nom"], $body["cat_obs"]);
            echo "Update Correcto";
        case "delete":
            $datos = $categoria->delete_categoria($body["cat_id"]);
            echo "Delete Correcto";
    break;

    }

?>