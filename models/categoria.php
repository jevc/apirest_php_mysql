<?php

    class Categoria extends Conectar {
        
        public function get_categoria() {

            $conectar = parent::conexion();
            parent::set_names();
            
            $sql="SELECT * FROM categoria WHERE est= 1";
            $sql=$conectar->prepare($sql);
            $sql->execute();

            return $resultado=$sql->fetchAll(PDO::FETCH_ASSOC);

        }

        public function get_categoria_x_id($catId) {

            $conectar = parent::conexion();
            parent::set_names();
            
            $sql="SELECT * FROM categoria WHERE est=1 AND cat_id=?";
            $sql=$conectar->prepare($sql);
            $sql->bindValue(1, $catId);
            $sql->execute();

            return $resultado=$sql->fetchAll(PDO::FETCH_ASSOC);

        }

        public function insert_categoria($catNom, $catObs) {

            $conectar = parent::conexion();
            parent::set_names();
            
            $sql="INSERT INTO categoria (cat_id, cat_nom, cat_obs, est) VALUES (NULL, ?, ?, '1') ";
            $sql=$conectar->prepare($sql);
            $sql->bindValue(1, $catNom);
            $sql->bindValue(2, $catObs);
            $sql->execute();

            return $resultado=$sql->fetchAll(PDO::FETCH_ASSOC);

        }

        public function update_categoria($catId, $catNom, $catObs) {

            $conectar = parent::conexion();
            parent::set_names();
            
            $sql="UPDATE categoria SET cat_nom = ?, cat_obs = ? WHERE cat_id = ?";
            $sql=$conectar->prepare($sql);
            $sql->bindValue(1, $catNom);
            $sql->bindValue(2, $catObs);
            $sql->bindValue(3, $catId);
            $sql->execute();

            return $resultado=$sql->fetchAll(PDO::FETCH_ASSOC);

        }

        public function delete_categoria($catId) {

            $conectar = parent::conexion();
            parent::set_names();
            
            $sql="UPDATE categoria SET est = '0' WHERE cat_id = ?";
            $sql=$conectar->prepare($sql);            
            $sql->bindValue(1, $catId);
            $sql->execute();

            return $resultado=$sql->fetchAll(PDO::FETCH_ASSOC);

        }


    }
?>